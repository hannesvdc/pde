function U = phasefield(a, b, J, tf, N, c, g, f1, f2)
    % setup mesh parameters
    dx = (b-a)/J;
    dt = tf/N;
    mu = dt/(dx*dx);
    
    % Setup variables for timestepping
    U = zeros(J+1,1);
    j = 2:1:J;
    
    % Apply initial conditions and boundary conditions
    U(1) = f1(0.0);
    U(J+1) = f2(0.0);
    U(j) = g((j-1)*dx);
    
    % Timestep until the final time
    for n = 1:N
        % Calculate interior points
        U(j) = c*mu*U(j+1) + c*mu*U(j-1) +...
            (1 - 2*c*mu + dt)*U(j) - dt*U(j).^3;
        
        % Apply boundary conditions
        U(1) = f1(n*dt);
        U(J+1) = f2(n*dt);
    end
end