function U = implicit_euler(a, b, J, tf, N, g, f1, f2)
% IMPLICIT_EULER Solve the heat equation in 1D with Dirichlet boundary
% conditions with the implicit Euler method.
%   U = IMPLICIT_EULER(a, b, J, tf, N, g, f1, f2) solves the heat equation
%   on mesh [a,b] in x, at time tf. Use J+1 mesh points and N timesteps. 
%   Begin condition is function g, boundary conditions at x = a: f1 and 
%   at x = b: f2.

    % Calculate mesh parameters and points
    dx = (b-a)/J;
    dt = tf/N;
    mu = dt/dx^2;
    x = linspace(a, b, J+1)';
    
    % Apply initial conditions and boundary condition
    U =  g(x);
    U(1) = f1(0);
    U(J+1) = f2(0);
    
    % Compute tridiagonal matrix
    a = - mu*ones(J-2,1);
    c = a;
    b = (1+2*mu)*ones(J-1,1);
    
    % Timestep until final time
    for n = 1:N
        % Apply boundary conditions
        U(1) = f1(n*dt); 
        U(J+1) = f2(n*dt);
               
        % Set up RHS for the system of equations
        d = U(2:J);
        d(1) = d(1) + mu*U(1);
        d(J-1) = d(J-1) + mu*U(J+1);
        
        % Solve triangular system of equations        
        U(2:J) = thomas(a, b, c, d);
    end
end