function U = thomas(a, b, c, d)
% THOMAS Solve the tridiagonal system of equations of the heat equation 
% in 1D with Dirichlet boundary conditions with the the Thomas algorithm.
%   U = THOMAS(a, b, c, d) solves the tridiagonal system of equations 
%   of the heat equation with given subdiagonal a, principal diagonal b, 
%   superdiagonal c and right-hand side d. 
n = length(d);
e = zeros(n,1);
f = zeros(n,1);
U = zeros(n,1);
e(1) = c(1)/b(1);
for j = 2:(n-1)
    e(j) = c(j)/(b(j)-a(j-1)*e(j-1));
end
f(1) = d(1)/b(1);
for j = 2:n
    f(j) = (d(j)-a(j-1)*f(j-1))/(b(j)-a(j-1)*e(j-1)); 
end
U(n) = f(n);
for j = (n-1):-1:1
    U(j) = f(j)-e(j)*U(j+1);
end
end