% Order test for the Crank-Nicolson method
a = 0.0;
b = 1.0;
J = 10;
mu = 0.01;
tf = 1.0;
f1 = @(x) 0.0;
f2 = @(x) 0.0;
g = @(x) sin(pi*x);

exact = @(x,t) exp(-pi^2*t)*sin(pi*x);
error = [];
time = [];

% calculate first approximation
dx = (b-a)/J;
dt = 0.0001;
N = tf/dt;

for i = 1:1:8
    J = 2*J;
    dx = (b-a)/J;
    N = floor(tf/dt);
    tf = N*dt;
    U = crank_nicolson(a, b, J, tf, N, g, f1, f2);
    
    % Interpolate the error on the finer mesh
    % to calculate the pointwise difference on 
    % points of the coarser mesh.
    j=linspace(0,1,J+1)';
    err = max(abs(U-exact(j, tf)));
    error = [error, err];
    time = [time, dx];

    Uprev = U;
end

figure(1);
subplot(1,2,2)
loglog(time, error);
title('Second order convergence in space');
xlabel('dx');
ylabel('Maximal error');