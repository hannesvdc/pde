% Order test for the explicit Euler method
a = 0.0;
b = 1.0;
J = 10;
mu = 0.25;
tf = 1.0;
f1 = @(x) 0.0;
f2 = @(x) 0.0;
g = @(x) sin(pi*x);

exact = @(x,t) exp(-pi^2*t)*sin(pi*x);
error = [];
place = [];

% calculate first approximation
dx = (b-a)/J;
dt = mu*dx*dx;
N = tf/dt;
Uprev = explicit_euler(a, b, J, tf, N, g, f1, f2);

for i = 1:1:6
    disp(i);
    J = 2*J;
    dx = (b-a)/J;
    dt = mu*dx*dx;
    N = tf/dt;
    U = explicit_euler(a, b, J, tf, N, g, f1, f2);
    
    % Interpolate the error on the finer mesh
    % to calculate the pointwise difference on 
    % points of the coarser mesh.
    j=linspace(0,1,J+1)';
    err = max(abs(U-exact(j,tf)));
    error = [error, err];
    place = [place, dx];

    Uprev = U;
end

figure(1);
subplot(1,2,2);
loglog(place, error);
title('Second order convergence in space');
xlabel('dx');
ylabel('Maximal error');