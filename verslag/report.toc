\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Implementation and order tests methods of the one-dimensional heat equation}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Explicit Euler method}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Implementation}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Correctness via order tests and example}{2}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Implicit Euler method}{3}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Code}{3}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Correctness via order tests and example}{4}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Crank-Nicolson method}{4}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Implementation}{4}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Correctness via order tests and example}{5}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}Thomas algorithm}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Application of implementation to two concrete examples}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Application of the explicit Euler method}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Application of implicit Euler method and Crank-Nicolson scheme}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Implementation and application of two nonlinear problems}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}The viscous Burgers' equation}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The phase-field equation}{10}{subsection.4.2}
