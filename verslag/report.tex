\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{float}
\usepackage{listings}

\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9} % for two column documents
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9} % for two column documents
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1}   
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}

\date{Academic year 2016-2017}
\address{
	Master Mathematical Engineering \\
	Numerical methods for solving PDEs \\
	Prof. Dr. Ir. Stefan Vandewalle, Prof.Dr. Stefaan Poedts}
\title{Practicum 1: Numerical solution of the heat equation}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
	
\lstset{language=Matlab,%
	%basicstyle=\color{red},
	breaklines=true,%
	linewidth=14cm,
	basicstyle=\scriptsize,
	morekeywords={matlab2tikz},
	keywordstyle=\color{blue},%
	morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
	identifierstyle=\color{black},%
	stringstyle=\color{mylilas},
	commentstyle=\color{mygreen},%
	showstringspaces=false,%without this there will be a symbol in the places where there is a space
	numbers=left,%
	numberstyle={\tiny \color{black}},% size of the numbers
	numbersep=9pt, % this defines how far the numbers are from the text
	emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
	%emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

	\maketitle
	
	\tableofcontents
	
\section{Introduction}
This report shows an implementation and order tests of the explicit Euler, implicit Euler and Crank-Nicolson method for solving the one-dimensional, time dependent heat equation and two nonlinear problems (the viscous Burgers' equation and the phase-field equation). We also briefly discuss an implementation of the Thomas algorithm. 

We show the application of the methods above on four examples and discuss numerical properties (stability, accuracy, \dots).

\section{Implementation and order tests methods of the one-dimensional heat equation}
\label{sec:intro}
This section enlists three numerical schemes for solving the one-dimensional, time dependent heat equation with Dirichlet boundary conditions. The heat transfer model is given by
\[
\frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}, x \in [0,1], t \geq 0
\]
with initial value $u(x, 0) = g(x)$ and boundary conditions $u(0, t) = f_1(t), u(1, t) = f_2(t)$. The function $u$ describes the temperature at position $x$ and time $t$.
The following subsections describe the explicit Euler, implicit Euler and Crank-Nicolson scheme which we implemented in MATLAB. We also show the correctness of the code by numerically determining the order of convergence in space and time. This gives enough confirmation that our code is indeed correct. We determine the order of the method by applying our implementation to the problem with initial condition $g(x) = \sin(\pi x)$ and boundary conditions $f_1(t) = f_2(t) = 0.$ Then we know the exact solution $u(x,t) = \exp(-\pi^2 t) \sin(\pi x)$ and this makes it possible to evaluate and plot the error.

We also test the method on the heat equation with boundary condition $u(0,t) = f_1(t) = 0, u(1, t) = f_2(t) = 1$ and initial condition $u(x, 0) = g(x) = x$. This gives the exact solution $u(x,t) = x$. This function is linear in space, such that the truncation error $T(x,t)$ becomes zero, because each derivative in time is zero and each second order derivative in space is zero. Each method should return the exact solution, with an error of order machine precision.

\subsection{Explicit Euler method}
The most straightforward method for the heat equation is the explicit Euler method. It consists of forward differences to approximate the temporal derivative and central differences for the spatial derivative. More accurately, the method is given by
\[
U^{n+1}_j = (1+\mu\delta^2x)U^{n}_j
\]
Where the parameter $\mu$ is defined as $\frac{\Delta t}{\Delta x^2}$. It can be shown that the maximal error behaves as $\mathcal{O}(\Delta t, \Delta x^2)$, but the method is only stable when $\mu \leq \frac{1}{2}$.

\subsubsection{Implementation}
This section contains the MATLAB code for the explicit Euler method.
\lstinputlisting{../explicit_euler.m}

\subsubsection{Correctness via order tests and example}
Now we turn to checking the correctness of the code via order tests. Important is that we always have to keep either $\Delta t$ or $\Delta x$ constant. The plots in Figure \ref{fig:conveuler} show the convergence.
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{expliciteulerconvergence}
	\caption{The convergence order of the explicit Euler method. First order in time, second order in space.}
	\label{fig:conveuler}
\end{figure}
From this figure we can clearly conclude first order convergence in time and second order in space as the theory dictates.

If we choose $\mu = 0.25$ and $J = 10$, we obtain a maximal error of $\num{1.11e-16}$ at $t_F = 1 s$ for the example described at the end of the introduction of Section \ref{sec:intro}. This confirms the correctness of the implementation.

\subsection{Implicit Euler method}
The implicit Euler method is very similar to the explicit method. The only difference is that the discrete scheme is evaluated at time $n+1$ and the temporal derivative is approximated by backward differences. More specifically
\[
(1-\mu\delta^2x)U^{n+1}_j = U^{n}_j.
\]
It can also be shown that this method is stable for all possible values of $\mu$ and that the maximal error behaves as $\mathcal{O}(\Delta t, \Delta x^2)$.

\subsubsection{Code}

The implementation for the implicit Euler method.

\lstinputlisting{../implicit_euler.m}

\subsubsection{Correctness via order tests and example}
Figure \ref{fig:order_test_implicit_euler} confirms that the order of the implicit Euler method is  $\mathcal{O}(\Delta t, \Delta x^2)$.

\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{order_test_implicit_euler}
	\caption{The convergence order of the implicit Euler method. First order in time, second order in space.}
	\label{fig:order_test_implicit_euler}	
\end{figure}

We tested the example of the end of the introduction of Section \ref{sec:intro} af $t_F = 0.5 s$ with $\mu = 0.25$ and $J = 20$. We obtained a maximal error of $\num{2.22e-16}$. This confirms the correctness of the implementation.

\subsection{Crank-Nicolson method}
A very attractive scheme is the so called Crank-Nicolson scheme, which is a combination of the explicit and implicit Euler method with weighting factor $\theta = \frac{1}{2}$. This method is given by
\[
(1-\frac{\mu}{2}\delta^2x)U^{n+1}_j = (1+\frac{\mu}{2}\delta^2x)U^{n}_j.
\]
This is also an implicit scheme, but it can be shown that this method is of second order accuracy in space and time.

\subsubsection{Implementation}
We now list the MATLAB code for this method.
\lstinputlisting{../crank_nicolson.m}
\subsubsection{Correctness via order tests and example}
The plots in Figure \ref{fig:convcn} show second order convergence in space and in time. Here it is important to note that while carrying out convergence tests, it is necessary to keep either $\Delta t$ or $\Delta x$ constant. Suppose otherwise that we keep $\mu$ constant such that $\Delta t = \mu \Delta x^2$. Then the convergence order is $\mathcal{O}(\Delta t, \Delta x^2) = \mathcal{O}(\Delta t, \Delta t) = \mathcal{O}(\Delta t)$ so that we only get first order converge in time (and second in space). This can be seen on Figure \ref{fig:cnmu}.

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{cranknicolsonconvergence}
	\caption{The convergence orders of Crank-Nicolson in space and time. Both are second order.}
	\label{fig:convcn}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{cnmu}
	\caption{The convergence order of Crank-Nicolson when we keep $\mu$ constant. This yields first order in time and second in space.}
	\label{fig:cnmu}
\end{figure}

If we choose $\mu = 0.01$ and $J = 10$, we obtain a maximal error of $\num{6.0e-15
}$ for the experiment of the end of the introduction of Section \ref{sec:intro}. This confirms the correctness of the implementation.

\subsection{Thomas algorithm}
Both the implicit Euler and Crank-Nicolson scheme require to solve a tridiagonal system on each time step. To solve these systems rapidly, we implemented the Thomas algorithm (because of the page limit, we could not include the code listing).

Correctness of the implementation was tested rigorously. Figure \ref{fig:comparison_performance_thomas} compares the performance of the Thomas algorithm and the standard backslash operator of MATLAB. This confirms that the Thomas algorithm is way faster for tridiagonal systems of equations ($\mathcal{O}(J)$ FLOPS against  $\mathcal{O}(J^3)$ FLOPS).

\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{comparison_performance_thomas}
	\caption{Comparison execution time Thomas algorithm and backslash operator of MATLAB.}
    \label{fig:comparison_performance_thomas}	
\end{figure}

\section{Application of implementation to two concrete examples}
This section shows the results of the previous three methods on a given situation. First we use the explicit Euler method, then we compare the implicit Euler method and the Crank-Nicolson scheme.
\subsection{Application of the explicit Euler method}
Consider the heat propagation problem with boundary conditions $f_1(t) = 0$, $f_2(t) = 1$ and initial value $g(x) = \sin(5\pi x/2)$. As spacial step, we use $\Delta x = 1/20$ and as time step $\Delta t = 0.001$. We choose a stable value for $\mu = 0.4$. Figure \ref{fig:opgave2} shows the numerical results. From the figure we conclude that the temperature distribution approximates a straight line as $t$ goes to infinity. This result is numerically stable.
\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{opgave2}
	\caption{The solution the heat equation on times $t_F=0.05$ and $t_F = 0.5$ seconds for $\mu=0.4$. The solution is stable and converges nicely.}
	\label{fig:opgave2}
\end{figure}
If however, we choose as a time step $\Delta t = 0.0013$ then $\mu=0.51$ which is just above the stability limit. Figure \ref{fig:eulerunstable} shows the numerical result for the same problem.
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{eulerunstable}
	\caption{The solution the heat equation on times $t_F=0.05$ and $t_F = 0.5$ seconds for $\mu=0.51$.We can clearly see that the solution explodes due to the instability of the scheme (hence the scale of the y-axis).}
	\label{fig:eulerunstable}
\end{figure}

\subsection{Application of implicit Euler method and Crank-Nicolson scheme}
Consider again the problem with initial conditions $g(x)= \sin(\pi x)$ and boundary conditions $f_1(t) = f_2(t) = 0$. Then the exact solution is $u(x,t) = e^{-\pi^2 t} \sin(\pi x)$. We now apply the implicit Euler and Crank-Nicolson method to this problem with step sizes $\Delta x = 1/10, 1/20, 1/40, 1/80$ and $\Delta t = 1/20, 1/40, 1/80, 1/160, 1/320, 1/640$. Tables \ref{tab:errimp} and \ref{tab:errcn} show the values of the maximal error.

\begin{table}
	\begin{adjustwidth}{-0.5cm}{}
		\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		$\Delta x \backslash \Delta t$ & $1/20$ & $1/40$ &$1/80$ &$1/160$ &$1/320$ & $1/640$ \\
		\hline 			
		$1/10 $ & \num{1.1420e-02} & \num{5.3564e-03} & \num{2.6876e-03} & \num{1.4557e-03} & \num{8.6698e-04} & \num{5.7958e-04} \\
		$1/20 $ & \num{1.1045e-02} & \num{5.0547e-03} & \num{2.4244e-03} & \num{1.2122e-03} & \num{6.3340e-04} & \num{3.5098e-04} \\
		$1/40 $ & \num{1.0953e-02} & \num{4.9802e-03} & \num{2.3595e-03} & \num{1.1523e-03} & \num{5.7590e-04} & \num{2.9471e-04} \\
		$1/80 $ & \num{1.0929e-02} & \num{4.9616e-03} & \num{2.3434e-03} & \num{1.1373e-03} & \num{5.6158e-04} & \num{2.8070e-04} \\
		\hline
		\end{tabular}
		\end{adjustwidth}
	\caption{Maximal error for the implicit Euler method in function of $\Delta x$ and $\Delta t$.}
	\label{tab:errimp}
\end{table}
\begin{table}
	\begin{adjustwidth}{-0.5cm}{}
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		$\Delta x \backslash \Delta t$ & $1/20$ & $1/40$ &$1/80$ &$1/160$ &$1/320$ & $1/640$ \\
		\hline 
		$1/10 $ &$\num{4.25e-4}$ & $\num{1.1458e-4}$&$\num{2.5121e-4}$&$\num{2.8547e-4}$&$\num{2.9405e-4}$&$\num{2.9619e-4}$ \\
		$1/20 $ &$\num{6.3984e-4}$ & $\num{1.0688e-4}$&$\num{2.8130e-5}$&$\num{6.1989e-5}$&$\num{7.0461e-5}$&$\num{7.2579e-5}$ \\
		$1/40 $ &$\num{6.9265e-4}$ & $\num{1.6136e-4}$&$\num{2.6758e-5}$&$\num{7.0006e-6}$&$\num{1.5447e-5}$&$\num{1.7559e-5}$ \\
		$1/80 $ &$\num{7.0580e-4}$ & $\num{1.749e-4}$&$\num{4.0425e-5}$&$\num{6.6919e-6}$&$\num{1.7482e-6}$&$\num{3.8586e-6}$ \\
		\hline
	\end{tabular}
	\end{adjustwidth}
\caption{Maximal error for the Crank-Nicolson method in function of $\Delta x$ and $\Delta t$.}
\label{tab:errcn}
\end{table}
From Table \ref{tab:errimp} we can conclude that the convergence of the implicit Euler method is first order in time (as it is for the consistency order). Consider for example the final row: we see that the error approximately halves each time step (as the time step does). This behaviour can be seen in each row. In the same table, we don't clearly see second order convergence in space. This is due to the fact that we already almost reached the minimal error with the given temporal discretization. The convergence plot in Figure \ref{fig:order_test_implicit_euler} does show second order.

The same conclusions can be drawn for Crank-Nicolson in Table \ref{tab:errcn}. In the final row the second order convergence in time is visible (the same as the consistency order) (the error goes down with the factor $\frac{1}{4}$ for every halving step). But now we also see second order convergence in space in the final column, meaning that we haven't reached the minimal error for the given time step yet. This results confirms the theory. 

This result confirms that the accuracy (convergence) is determined by the consistency order.

\section{Implementation and application of two nonlinear problems}
We now adapt our explicit Euler scheme to solve nonlinear equations. We consider the Burger's equation and the phase-field equation.

\subsection{The viscous Burgers' equation}
The nonlinear viscous Burgers' equation is given by 
\[
\frac{\partial u}{\partial t} = b \frac{\partial^2 u}{\partial x^2} - u\frac{\partial u}{\partial x}.
\]
The discretization of the PDE, regarding explicit Euler, becomes:
\[
U^{n+1}_j = (1-\nu(U^{n}_{j}-U^{n}_{j-1})+b\mu\delta_x^2)U^{n}_{j},
\]
with $\mu = \frac{\Delta t}{\Delta x^2}$ and $\nu = \frac{\Delta t}{\Delta x}$.

We use backward differences to discretize $\frac{\partial u}{\partial x}$ to avoid convergence problems with double distance differences and because we expect a wave front propagating to the right. In order to make sure that $1-2\mu b - \nu U_{j}^{n} \geq 0$, we choose the number of timesteps big enough (e.g. 10000) (such that $\Delta t$ is small) such that $\mu$ and $\nu$ are small enough.

We illustrate the implementation by solving the problem with initial value $g(x) = |\text{sin}(2\pi x)|$ and boundary conditions $f_1(t) = f_2(t) = 0$ over the time interval $[0,1]$. Figure \ref{fig:burgersequation} shows six plots, one plot per $0.2$ seconds. We indeed see the propagation of a wave front to the right.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{burgersequation}
	\caption{The numerical solution of the viscous Burgers' equation on 
		times $t_F=0, \dots, 1$ seconds.}
	\label{fig:burgersequation}
\end{figure}

\subsection{The phase-field equation}
Another nonlinear problem is the so-called phase-field equation. The PDE is given by
\[
\frac{\partial u}{\partial t} = b \frac{\partial^2 u}{\partial x^2} - (u^3-u).
\]
The explicit Euler method can easily be adapted to solve this equation. This discretization is then given by
\[
U^{n+1}_j = (1+b\mu\delta_x^2)U^{n}_j - \Delta t({(U^{n}_j)}^3 - U^{n}_j),
\]
with the same value of $\mu = \frac{\Delta t}{\Delta x^2}$.

We illustrate the solution for the problem with initial value $g(x) = \text{sinc}(3\pi x^2)$ and boundary conditions $f_1(t) = f_2(t) = 0$ over the time interval $[0,5]$. Figure \ref{fig:phasefield} shows six plots, one plot per second.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{phasefield}
	\caption{The numerical solution of the phase-field equation on 
		times $t_F=0, \dots, 5$ seconds.}
	\label{fig:phasefield}
\end{figure}
\end{document}