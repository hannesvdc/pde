function U = viscoucs_Burgers(a, b, J, tf, N, g, f1, f2, c)
    dx = (b-a)/J;
    dt = tf/N;
    mu = dt/dx^2;
    nu = dt/dx;   
    converg = 1-2*c*mu-nu;
    disp(converg)
    disp(mu)
    disp(nu)
    
    % c = b of the equation
    x = linspace(a,b, J+1)';
    j = 2:1:J;
    U = g(x);
    
    for n = 1:N
        U(j) = (1-2*c*mu)*U(j)+c*mu*(U(j-1)+U(j+1)) - nu*U(j).*(U(j)-U(j-1));
        U(1) = f1(n*dt); U(J+1) = f2(n*dt);
    end
end