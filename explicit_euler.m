function U = explicit_euler(a, b, J, tf, N, g, f1, f2)
% EXPLICIT_EULER Solve the heat equation in 1D with Dirichlet boundary
% conditions with the explicit Euler method.
%   U = EXPLICIT_EULER(a, b, J, tf, N, g, f1, f2) solves the heat equation
%   on mesh [a,b] in x, at time tf. Use J+1 mesh points and N timesteps. 
%   Begin condition is function g, boundary conditions at x = a: f1 and 
%   at x = b: f2.

    % Calculate mesh parameters
    dx = (b-a)/J;
    dt = tf/N;
    mu = dt/dx^2;
    
    % Notify if the scheme is numerically unstable
    if mu > 0.5 
        disp('The scheme is numerically unstable. Choose a smaller timestep or coarser mesh.');
    end
   
    % Setup variables for timestepping
    U = zeros(J+1,1);
    j = 2:1:J;
    
    % Apply initial conditions and boundary conditions
    U(1) = f1(0);
    U(J+1) = f2(0);
    U(j) = g((j-1)*dx);

    % Timestep until the final time
    for n= 1:N        
        % Calculate the solution on inner mesh points
        U(j) = mu*U(j-1) + (1-2.*mu)*U(j) + mu*U(j+1);
        
        % Apply boundary conditions
        U(1) = f1(n*dt);
        U(J+1) = f2(n*dt);
    end
end