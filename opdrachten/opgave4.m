a = 0;
b = 1;
J = 100;
c = 0.005;
f1 = @(t) 0.0;
f2 = @(t) 0.0;
steps = 10^4;
g = @(x) abs(sin(2*pi*x));
x = linspace(0,1,J+1);

for j = 0:1:5
    tf = j*0.2;
    N = round(tf*steps-1);

    U = viscoucs_Burgers(a, b, J, tf, N, g, f1, f2, c);
    subplot(3,2, j+1);
    plot(x, U);
    title(['tf = ', num2str(tf)]);
end