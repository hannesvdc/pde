% Order test for the explicit Euler method
a = 0.0;
b = 1.0;
tf = 0.5;
f1 = @(t) 0.0;
f2 = @(t) 0.0;
g = @(x) sin(pi*x);

exact = @(x,t) exp(-pi^2*t)*sin(pi*x);
error = [];
time = [];

J = 5;
errori = zeros(4,6);
errorcn = zeros(4, 6);

for k = 1:1:4
    J = 2*J;
    x = linspace(0,1,J+1)';
    N = 5;
    for l = 1:1:6
        disp(k);
        N = 2*N;

        Ui = implicit_euler(a, b, J, tf, N, g, f1, f2);
        errori(k, l) = max(abs(Ui - exact(x,tf)));
        
        Ucn = crank_nicolson(a, b, J, tf, N, g, f1, f2);
        errorcn(k, l) = max(abs(Ucn - exact(x,tf)));

    end
end