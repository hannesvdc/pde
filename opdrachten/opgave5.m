a = 0.0;
b = 1.0;
J = 100;
c = 10^-4;
g = @(x) sinc(3*pi*x.*x);
f1 = @(t) 0.0;
f2 = @(t) 0.0;
steps = 100;
x = linspace(0,1,J+1);

for tf = 0:1:5
    N = tf*steps-1;
    U = phasefield(a, b, J, tf, N, c, g, f1, f2);
    
    subplot(3,2, tf+1);
    plot(x, U);
    title(['tf = ', num2str(tf)]);
    xlabel('x');
    ylabel('u(x, tf)');
end