% Explicit Euler
a = 0.0;
b = 1.0;
J = 20;
g = @(x) sin(5*pi/2);
f1 = @(t) 0.0;
f2 = @(t) 1.0;
tf = 0.05;
dt = 0.0013;
N = tf/dt;

U = explicit_euler(a, b, J, tf, N, g, f1, f2);
x = linspace(0,1,J+1);
figure(1);
subplot(1,2,1);
plot(x, U);
title('Solution at tf = 0.05 seconds');
xlabel('x');
ylabel('u(x, 0.05)');

tf = 0.5;
N = tf/dt;
U = explicit_euler(a, b, J, tf, N, g, f1, f2);
x = linspace(0,1,J+1);
subplot(1,2,2);
plot(x, U)
title('Solution at tf = 0.5 seconds');
xlabel('x');
ylabel('u(x, 0.5)');