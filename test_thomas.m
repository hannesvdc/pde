% Compare thomas and the backslash operator "\" in Matlab for the 
% solution of a tridiagonal system of equations of the heat equation
means_slash = [];
means_thomas = [];
sizes = [];

for k = 2:0.2:4
    J = round(10^k);
    disp(J);
    sizes = [sizes, J];
    
    % Generate random right hand side d
    d = randn(J-1,1);
    mu = 0.4;
    T = (1+2*mu)*diag(ones(J-1,1)) - mu*(diag(ones(J-2,1),1)...
             + diag(ones(J-2,1),-1));
    a = - mu*ones(J-2,1);
    c = a;
    b = (1+2*mu)*ones(J-1,1);
    nb_it = 10;
    
    % Measure the elapsed time for standard gaussian elimination and
    % average over 10 measurements.
    elapsed_time = 0;
    for i = 1:nb_it
        tic;
        res = T\d;
        elapsed = toc;
        elapsed_time  = elapsed_time + elapsed;
    end
    mean = elapsed_time/nb_it;
    means_slash = [means_slash, mean];
    
    % Also measure the elapsed time for the thomas algorithm and 
    % average over 10 measurements.
    elapsed_time = 0;
    for i = 1:nb_it
        tic;
        res_thomas = thomas(a, b, c, d);
        elapsed = toc;
        elapsed_time  = elapsed_time + elapsed;
    end
    mean = elapsed_time/nb_it;
    means_thomas = [means_thomas, mean];
    disp(norm(res-res_thomas))
end

% Make a plot of the running time for gaussian elimination and thomas
% algorithm
figure(1);
loglog(sizes, means_slash);
hold on;
loglog(sizes, means_thomas);
title('Comparison \-operator in Matlab and Thomas-algorithm for tridiagonal system of equations')
legend('Matlab backslash O(J^3)', 'Thomas algorithm O(J)');
xlabel('J')
ylabel('execution time')

