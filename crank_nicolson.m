function U = crank_nicolson(a, b, J, tf, N, g, f1, f2)
% CRANK_NICOLSON Solve the heat equation in 1D with Dirichlet boundary
% conditions with the Crank-Nicolson method.
%   U = CRANK_NICOLSON(a, b, J, tf, N, g, f1, f2) solves the heat equation
%   on mesh [a,b] in x, at time tf. Use J+1 mesh points and N timesteps. 
%   Begin condition is function g, boundary conditions at x = a: f1 and 
%   at x = b: f2.

    % Calculate mesh parameters
    dx = (b-a)/J;
    dt = tf/N;
    mu = dt/dx^2;
    theta = 0.5;

    % Setup variables for timestepping
    U = zeros(J+1,1);
    j = 2:1:J;
    
     % Apply initial conditions and boundary conditions
    U(1) = f1(0);
    U(J+1) = f2(0);
    U(j) = g((j-1)*dx);
    
    % Compute tridiagonal matrix
    a = -theta*mu*ones(J-2,1);
    c = a;
    b = (1.0 + 2.0*theta*mu)*ones(J-1,1);
    
    % Timestep until the final time
    for n = 1:N
        % Apply boundary conditions
        U(1) = f1(n*dt);
        U(J+1) = f2(n*dt);
        
        % Set up RHS for the system of equations
        d = (1.0-theta)*mu*U(j-1) + (1.0 - 2.0*(1.0-theta)*mu)*U(j)...
            + (1.0-theta)*mu*U(j+1);
        d(1) = d(1) + theta*mu*U(1);
        d(J-1) = d(J-1) + theta*mu*U(J+1);
        
        % Solve the triangular system of equations
        U(j) = thomas(a, b, c, d);        
    end
    
end